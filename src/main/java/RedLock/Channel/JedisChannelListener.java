package RedLock.Channel;

import RedLock.Connection.JedisConnectionManager;
import RedLock.ThreadUtil;
import lombok.extern.slf4j.Slf4j;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
public class JedisChannelListener {

    private String unlockedMessagePattern;
    private String lockName;
    private JedisConnectionManager connectionManager;

    private JedisPubSub jedisPubSub;
    private Jedis jedis;

    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private Set<Long> subscribers = new HashSet<>();
    private Semaphore notificationResource = new Semaphore(0);
    private AtomicBoolean hasNotification = new AtomicBoolean(false);

    public JedisChannelListener(
            String unlockedMessagePattern,
            String channelName,
            JedisConnectionManager connectionManager
    ) {
        this.unlockedMessagePattern = unlockedMessagePattern;
        this.lockName = channelName;
        this.connectionManager = connectionManager;
    }

    public void startListening() {

        String resourceId = ThreadUtil.getName();
        jedisPubSub = new JedisPubSub() {
            public void onMessage(String channel, String message) {
                log.debug("Thread {} get message {} in channel {}", resourceId, message, channel);
                if (message.startsWith(unlockedMessagePattern)) {
                    onGettingNewMessage();
                }
            }
        };

        jedis = connectionManager.borrow(resourceId);

        executorService.submit(() -> {
            jedis.subscribe(jedisPubSub, lockName);
        });

    }

    public void waitForGettingNotificationFromChannel(long timeout) throws InterruptedException {
        boolean acquirerResource = notificationResource.tryAcquire(timeout, TimeUnit.MILLISECONDS);
        log.debug("Thread {} notified", Thread.currentThread().getId());
        if (acquirerResource) {
            hasNotification.set(false);
        }
    }

    public void shutdown() {
        try {
            jedisPubSub.unsubscribe();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            executorService.shutdownNow();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String resourceName = ThreadUtil.getName();
        connectionManager.returnBack(resourceName, jedis);
    }

    public void addSubscriber(long subId) {
        subscribers.add(subId);
    }

    public void onGettingNewMessage() {
        if (hasNotification.compareAndSet(false, true)) {
            notificationResource.release();
        }
    }

    public void removeSubscriber(long subscriberId) {
        subscribers.remove(subscriberId);
    }

    public boolean isSubscribersEmpty() {
        return subscribers.isEmpty();
    }

}
