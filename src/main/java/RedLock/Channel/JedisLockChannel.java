package RedLock.Channel;

import RedLock.Connection.JedisConnectionManager;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
public class JedisLockChannel {
    //TO-DO: refactor to common channel
    private JedisConnectionManager connectionManager;
    private ConcurrentHashMap<String, JedisChannelListener> lockNameChannelInfo = new ConcurrentHashMap<>();
    private String unlockedMessagePattern;

    public JedisLockChannel(JedisConnectionManager connectionManager, String unlockedMessagePattern) {
        this.connectionManager = connectionManager;
        this.unlockedMessagePattern = unlockedMessagePattern;
    }

    public void subscribe(String lockName) {
        lockNameChannelInfo.compute(lockName, (key, channelListener) -> {
            Long threadId = Thread.currentThread().getId();
            log.debug("Thread {} subscribe channel {}", threadId, lockName );
            if (channelListener == null) {
                channelListener = new JedisChannelListener(unlockedMessagePattern, lockName, connectionManager);
                channelListener.startListening();
            }
            channelListener.addSubscriber(threadId);
            return channelListener;
        });
    }

    public void waitForNotification(String lockName, long timeOutMillis) throws InterruptedException {
        lockNameChannelInfo.compute(lockName, (key, channelListener) -> {
            if (Objects.isNull(channelListener)) {
                throw new RuntimeException("Cannot have any channel with name = " + lockName);
            }
            return channelListener;
        });

        lockNameChannelInfo.get(lockName).waitForGettingNotificationFromChannel(timeOutMillis);
    }

    public void unSubscribe(String lockName) {
        lockNameChannelInfo.compute(lockName, (key, redisChannel) -> {
            long threadId = Thread.currentThread().getId();
            if (Objects.isNull(redisChannel)) {
                throw new RuntimeException("Cannot have channel with name " + lockName);
            };
            redisChannel.removeSubscriber(threadId);
            if (redisChannel.isSubscribersEmpty()) {
                 redisChannel.shutdown();
                 return null;
            }
            return redisChannel;
        });
    }

}
