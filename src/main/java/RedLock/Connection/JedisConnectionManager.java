package RedLock.Connection;

import RedLock.ThreadUtil;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;

public class JedisConnectionManager {

    private final AtomicInteger capacity =  new AtomicInteger(0);
    private final JedisPool connectionPool;
    private final Map<String, List<Jedis>> reservedConnections = new ConcurrentHashMap<>();

    public JedisConnectionManager(RedisConfig redisConfig) {
        this.capacity.set(redisConfig.getLockPoolSize());
        GenericObjectPoolConfig<Jedis>  lookPoolConfig = this.makePool(this.capacity.get());
        this.connectionPool =  new JedisPool(lookPoolConfig,
                redisConfig.getHostname(),redisConfig.getPort(),  redisConfig.getReadTimeOutLimit());

    }

     public GenericObjectPoolConfig<Jedis> makePool(int maxSize) {
         GenericObjectPoolConfig<Jedis> poolConfig = new GenericObjectPoolConfig<>();
         poolConfig.setTestWhileIdle(true);
         poolConfig.setNumTestsPerEvictionRun(-1);
         poolConfig.setMaxTotal(maxSize);
         return poolConfig;
     }

     public boolean reserve(String resourceId, int size) {
        AtomicBoolean reservedSuccessfully = new AtomicBoolean(false);
        capacity.updateAndGet(operand -> {
            int remaining = operand - size;
            if (remaining > 0) {
                reservedSuccessfully.set(true);
                return  remaining;
            } else {
                return operand;
            }
        });

        if (reservedSuccessfully.get()) {
            reservedConnections.putIfAbsent(resourceId, new ArrayList<>());
            for (int i = 0; i < size; i++) {
                 Jedis resource = connectionPool.getResource();
                 reservedConnections.get(resourceId).add(resource);
            }
        }
        return reservedSuccessfully.get();
     }

    public boolean reserve(final int size) {
        String connectionId = ThreadUtil.createUniqueName();
        return reserve(connectionId, size);
    }

    public <E> E doWithConnection(String resourceId, Function<Jedis, E> callBack){
        Jedis jedis = borrow(resourceId);
         try {
             return callBack.apply(jedis);
         } finally {
             returnBack(resourceId, jedis);
         }
    }

    public <E> E doWithConnection(Function<Jedis, E>  callBack) {
        String resourceId = ThreadUtil.getName();
        return doWithConnection(resourceId, callBack);
    }

    public Jedis borrow(String resourceId) {
        List<Jedis> returnList = new ArrayList<>();
        reservedConnections.compute(resourceId, (key, connections ) -> {
            if (Objects.isNull(connections)) {
                throw new RuntimeException("Invalid resource id");
            }
            if (connections.isEmpty()) {
                 throw new RuntimeException("No  any free connection");
            }
            Jedis jedis = connections.get(0);
            connections.remove(0);
            returnList.add(jedis);
            return connections;
        });
        return returnList.get(0);
    }

     public void returnBack(String resourceId, Jedis connection) {
        reservedConnections.compute(resourceId, (key, connections) -> {
            if (Objects.isNull(connections)) {
                throw new RuntimeException("Invalid resource id");
            };
            connections.add(connection);
            return connections;
         });
     }

     public void free(String resourceId) {
        reservedConnections.compute(resourceId, (key, connections) -> {
            if (Objects.isNull(connections)) {
                throw new RuntimeException("Invalid resource id");
            };
            if (!connections.isEmpty()) {
                connections.forEach(Jedis::close);
            }
            capacity.updateAndGet((current) -> current + connections.size());
            connections.clear();
            return null;
        });
     }

}
