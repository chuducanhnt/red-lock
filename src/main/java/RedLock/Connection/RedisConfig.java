package RedLock.Connection;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class RedisConfig {
    private String hostname;
    private Integer port;
    private Integer replicaCount;
    @Builder.Default
    private Integer lockPoolSize = 1000;
    @Builder.Default
    private Integer readTimeOutLimit = 2000;
    @Builder.Default
    private String unlockMessagePattern = "UN_LOCK";
}
