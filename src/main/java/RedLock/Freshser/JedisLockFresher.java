package RedLock.Freshser;

import RedLock.Connection.JedisConnectionManager;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class JedisLockFresher {

    private JedisConnectionManager jedisConnectionManager;
    private ScheduledExecutorService executor;

    public  JedisLockFresher(JedisConnectionManager jedisConnectionManager) {
        this.jedisConnectionManager = jedisConnectionManager;
        this.executor = Executors.newScheduledThreadPool(1);
    }

    public CompletableFuture<Void> start(String lockName, long period) {
        CompletableFuture<Void> result =  new CompletableFuture<Void>();
        executor.scheduleAtFixedRate(() -> {
            try {
                jedisConnectionManager.doWithConnection(jedis -> jedis.pexpire(lockName, period));
            } catch (Exception ex) {
                result.completeExceptionally(ex);
                throw ex;
            }
        }, period /3, period/3, TimeUnit.MILLISECONDS);
        return  result;
    }

    public void tryStop() {
        try {
             this.executor.shutdown();
        } catch (Exception exception) {
             exception.printStackTrace();
        }
    }

}
