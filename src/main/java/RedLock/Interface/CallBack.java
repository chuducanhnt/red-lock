package RedLock.Interface;

@FunctionalInterface
public interface CallBack {

    void execute();
}
