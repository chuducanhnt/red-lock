package RedLock;

import RedLock.Channel.JedisLockChannel;
import RedLock.Connection.JedisConnectionManager;
import RedLock.Connection.RedisConfig;
import RedLock.Freshser.JedisLockFresher;
import RedLock.Interface.CallBack;
import RedLock.Script.LuaScript;
import RedLock.Script.RedisResponse;
import jdk.jshell.spi.ExecutionControl;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Slf4j
public class RedLock {

    private final JedisConnectionManager connectionManager;
    private final JedisLockChannel lockChannel;
    private final RedisConfig redisConfig;
    private final ExecutorService operationExecutorService = Executors.newCachedThreadPool();

    public RedLock(String host, int port, int replicaCount) {
        this(RedisConfig.builder().hostname(host).port(port).replicaCount(replicaCount).build());
    }

    public RedLock(RedisConfig redisConfig) {
        this.redisConfig = redisConfig;
        this.connectionManager = new JedisConnectionManager(redisConfig);
        this.lockChannel = new JedisLockChannel(connectionManager, redisConfig.getUnlockMessagePattern());
    }

    void acquirer(String lockName, long expirationTimeMillis, CallBack callBack) {
        if (!connectionManager.reserve(2)) {
            throw new RuntimeException("Not have enough resource");
        }

        boolean getLockSuccessfully = getLock(lockName, expirationTimeMillis);

        if (!getLockSuccessfully) {
            try {
                lockChannel.subscribe(lockName);
                while (!getLockSuccessfully) {
                    long ttl = getTTL(lockName);
                    if (ttl > 0) {
                        lockChannel.waitForNotification(lockName, ttl + ttl / 10);
                    } else {
                        getLockSuccessfully = getLock(lockName, expirationTimeMillis);
                    }
                }
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException("Interrupted");
            } finally {
                lockChannel.unSubscribe(lockName);
            }
        }

        JedisLockFresher lockFresher = new JedisLockFresher(connectionManager);
        String resourceId = ThreadUtil.getName();
        try {
            CompletableFuture<Void> lockFresherFuture = lockFresher.start(lockName, expirationTimeMillis);
            CompletableFuture<Void> action = CompletableFuture.runAsync(callBack::execute, this.operationExecutorService);
            lockFresherFuture.exceptionally(throwable -> {
                action.completeExceptionally(throwable);
                return null;
            });
            action.join();
        } finally {
            lockFresher.tryStop();
            tryReleaseLock(lockName);
            tryNotifyOtherClients(lockName);
            connectionManager.free(resourceId);
        }

    }

    private boolean getLock(String lockName, long expirationTimeMillis) {
        final String lockValue = ThreadUtil.getName();
        log.debug("Try to get lock {}", lockName);
        try {
            Object response = connectionManager.doWithConnection(jedis -> {
                Object result = jedis.eval(LuaScript.GET_LOCK, 1, lockName, lockValue, String.valueOf(expirationTimeMillis));
                return result;
            });
            log.debug("Get lock {} : {}", lockName, !RedisResponse.isFailed(response));
            return !RedisResponse.isFailed(response);
        } catch (Exception exception) {
            log.error("Get lock error", exception);
            releaseLock(lockName);
            throw exception;
        }

    }

    private void tryReleaseLock(String lockName) {
        try {
            releaseLock(lockName);
        } catch (Exception ex) {
        }
    }

    private long getTTL(final String lockName) {
        return connectionManager.doWithConnection(jedis ->
        {
            long result = jedis.pttl(lockName);
            log.debug("Key {} have expire time : {}", lockName, result );
            return result;
        });
    }

    private void releaseLock(String lockName) {
        String lockValue = ThreadUtil.getName();
        connectionManager.doWithConnection(jedis -> {
             Object result =  jedis.eval(LuaScript.RELEASE_LOCK, 1, lockName, lockValue);
             log.debug("Release lock {} : {}", lockName, !RedisResponse.isFailed(result));
             return result;
        });
    }

    public void tryNotifyOtherClients(final String lockName) {
        try {
            connectionManager.doWithConnection(jedis -> jedis.publish(lockName, redisConfig.getUnlockMessagePattern()));
            log.debug("Notify message {}", lockName);
        } catch (Exception exception) {
            log.error("Notify other client", exception);
        }
    }

}
