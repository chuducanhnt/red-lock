package RedLock.Script;

import java.util.Objects;

public enum RedisResponse {


    SUCCESS("SUCCESS"),
    FAIL("FAIL");

    private String val;

    RedisResponse(String val) {
         this.val = val;
    }


    public static boolean isSuccessFull(final String response) {
        return SUCCESS.val.equalsIgnoreCase(response);
    }

    public static boolean isFailed(Object response) {
        return Objects.equals(RedisResponse.FAIL.val, response);
    }

}
