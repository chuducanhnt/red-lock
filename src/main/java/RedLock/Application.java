package RedLock;

import lombok.extern.slf4j.Slf4j;

import java.sql.Time;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Application {

    public static void main(String[] args) {
        final RedLock redLock = new RedLock("localhost", 6379, 1);
        Executor executor = Executors.newCachedThreadPool();
        for (int i = 0; i < 100; i++) {
            executor.execute(() -> {
                redLock.acquirer("duc-anh", 100000, () -> {
                    try {
                        TimeUnit.MILLISECONDS.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Acquirer ok");
                });
            });
        }
    }
}
