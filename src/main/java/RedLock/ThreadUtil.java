package RedLock;

import java.util.UUID;

public class ThreadUtil {
    private ThreadUtil() {
    }

    private static final InheritableThreadLocal<String> PARENT_THREAD_NAME = new InheritableThreadLocal<>();

    private static final String GENERATED_UUID = UUID.randomUUID().toString();

    public static String createUniqueName() {
        long threadId = Thread.currentThread().getId();
        PARENT_THREAD_NAME.set(threadId + ":" + GENERATED_UUID);
        return PARENT_THREAD_NAME.get();
    }

    public static String getName() {
        return PARENT_THREAD_NAME.get();
    }
}
